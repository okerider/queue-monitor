// gaet video wrapper size
var getVideoWrapperSize = function (setImage) {
  let size = {
    width : Math.round($('.site-video').width()),
    height: Math.round($('.site-video').height())
  }

  $('.site-video').append(`<img src="//placehold.it/${size.width}x${size.height}"/>`)
}

$(window).on('load', function () {
  /*
    set to true for covering background image with
    imgae placeholder with size text
  */
  getVideoWrapperSize(true)
})
